\select@language {polish}
\contentsline {chapter}{Spis tre\IeC {\'s}ci}{11}{section*.1}
\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{13}{chapter.1}
\contentsline {chapter}{\numberline {2}Wyb\IeC {\'o}r medium komunikacyjnego}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Dost\IeC {\k e}pne standardy komunikacji bezprzewodowej}{17}{section.2.1}
\contentsline {section}{\numberline {2.2}Wyb\IeC {\'o}r technologii Bluetooth}{18}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Wykorzystanie Bluetooth Low Energy}{18}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Wykorzystanie klasycznego Bluetooth'a}{19}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Opis przesy\IeC {\l }anych danych}{19}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Instrukcje danych}{20}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Instrukcje \IeC {\.z}\IeC {\k a}da\IeC {\'n}}{23}{subsection.2.3.2}
\contentsline {chapter}{\numberline {3}Projekt aplikacji typu serwer}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}ROS -- Robots Operating System}{25}{section.3.1}
\contentsline {section}{\numberline {3.2}Konfiguracja \IeC {\'s}rodowiska programistycznego}{26}{section.3.2}
\contentsline {section}{\numberline {3.3}Aplikacja typu serwer}{26}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Informacje wst\IeC {\k e}pne}{26}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Dane konfiguracyjne robota mobilnego}{27}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Om\IeC {\'o}wienie funkcjonowania programu}{29}{subsection.3.3.3}
\contentsline {chapter}{\numberline {4}\textit {BtClient} -- aplikacja typu klient}{35}{chapter.4}
\contentsline {section}{\numberline {4.1}Projekt ekran\IeC {\'o}w u\IeC {\.z}ytkownika}{35}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}\textit {MainActivity}}{36}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}\textit {ScanActivity}}{37}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}\textit {ControlPanelActivity}}{38}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}\textit {DataVisualizationActivity}}{41}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2}Spos\IeC {\'o}b funkcjonowania aplikacji}{42}{section.4.2}
\contentsline {chapter}{\numberline {5}Opis instalacji oprogramowania}{45}{chapter.5}
\contentsline {section}{\numberline {5.1}Aplikacja typu serwer}{45}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Instalacja wymaganych bibliotek}{45}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Konfiguracja adaptera Bluetooth}{46}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Dodawanie paczki \textit {bluetooth\textunderscore server} do \IeC {\'s}rodowiska ROS}{47}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Aplikacja typu klient}{48}{section.5.2}
\contentsline {chapter}{\numberline {6}Podsumowanie}{51}{chapter.6}
\contentsline {chapter}{Bibliografia}{53}{section*.26}
\contentsline {chapter}{Spis rysunk\IeC {\'o}w}{54}{section*.28}
