# The Bachelor's Thesis: The project and implementation of the application for Android devices to operate a mobile robot #

The purpose on which this thesis is focused on relates to methods of controlling wheeled mobile robots. Presented on the further pages of this paper project was divided into two main parts. The first part refers to a server application designed for the on-board computer of a mobile robot while the second one describes a client application that runs on an Android mobile device. The entire project is a proposal that allows the wireless control of a mobile robot providing basic capabilities for diagnosis and data visualization from the connected sensors. The solution is intended to provide comfortable use in a variety of robot workplaces.
	
The initial part of the paper concentrates on a communication medium. In the related chapter communication standards are overviewed based on those which are available in the analyzed mobile robots. However, due to comfortable use only wireless connections were taken into account. After analyzing all possibilities the Bluetooth technology was selected and its two varieties was tested - *Classic* and *Low Energy*. Next there is a short description of both of them. After that the author presents a method of transferring data between devices connected to each other with the *Classic Bluetooth* for which the RFCOMM protocol based on serial communication is used.
	
After selecting the desired communication medium the server application that runs on an on-board robot computer is presented. There is shown a need of a universal solution that can handle different hardware configurations of the supported robots. The author proposes a solution in the form of configuration files with a strictly defined structure that can be stored using the XML markup language. Then it is discussed how to get data from the connected sensors from the Robots Operating System (ROS) as well as a method of transforming that data to the base frame of a robot using *tf* mechanism. The end of the chapter contains a detailed explanation of the most important for the functioning of the server application code snippets.
	
Another section of the thesis is devoted to the client application developed for mobile devices running on the Android system. After a presentation of relevant information regarding the availability of the project there is a description of the user interface design as well as related functionalities. The information provided is intended to describe all possible situations which a user may face with during the use of the client application. Next the characteristic for the Android system life cycle of an activity (application view) is taken into consideration and described how it is managed in this paper.
	
Taking into account the ease of subsequent implementation of this project on real hardware there is a chapter that describes the requirements for a mobile robot computer and an Android device as well as instructions that lead to complete installation of both the server and client application.
	
The thesis shows the validity of the proposed solution that facilitates the process of controlling a wheeled mobile robot as well as indicates a potential of cooperation of the ROS and Android systems which can be used for further modifications and improvements of this project.

# Praca dyplomowa inżynierska: Projekt i realizacja aplikacji do obsługi robota mobilnego na urządzenia z systemem Android #

Zagadnienie, na którym skupiono się w niniejszej pracy dotyczy sposobów sterowania kołowymi robotami mobilnymi. Przedstawiony na kolejnych stronach projekt podzielony został na dwie zasadnicze części. Pierwszą stanowi aplikacja typu serwer przeznaczona na komputer pokładowy robota mobilnego, drugą natomiast program kliencki uruchamiany na urządzeniach mobilnych z systemem Android. Całość stanowić ma propozycję pozwalającą na sterowanie robotem mobilnym przy jednoczesnym zapewnieniu podstawowych możliwości diagnostyki i wizualizacji danych z podłączonych do niego sensorów zapewniającą komfortowe użytkowanie w różnych miejscach pracy robota.
	
Początkowy etap pracy porusza temat medium komunikacyjnego. W poświęconym temu tematowi rozdziale następuje przegląd dostępnych w badanych robotach mobilnych standardów komunikacyjnych przy czym ze względu na komfort pracy użytkownika uwaga skupiona jest na rozwiązaniach pozwalających na nawiązywanie połączeń bezprzewodowych. Po ich analizie, która ostatecznie prowadzi do wyboru technologii Bluetooth rozpatrzone zostają jego dwie odmiany -- *Classic* oraz *Low Energy*. Przybliżenie każdej z nich zwieńczone jest opisem przyjętego przez autora sposobu wymiany danych między urządzeniami połączonymi za pomocą *Classic Bluetooth*, dla którego wykorzystano protokół komunikacyjny RFCOMM będący emulacją portu szeregowego.
	
Po dokonaniu wyboru medium komunikacyjnego następuje przedstawienie projektu aplikacji typu serwer przeznaczonej dla komputera pokładowego robota mobilnego. Zwrócona tu zostaje uwaga na potrzebę jej uniwersalności ze względu na różne konfiguracje sprzętowe obsługiwanych robotów. Zdecydowano się na zaproponowanie rozwiązania w postaci plików konfiguracyjnych o ściśle zdefiniowanej strukturze zapisanej za pomocą języka znaczników XML. Omówiony został następnie sposób pobierania z systemu ROS danych z podłączonych sensorów jak również metoda ich transformacji do układu bazowego robota z wykorzystaniem mechanizmu *tf*. Koniec rozdziału zawiera wyjaśnienie najważniejszych z punktu widzenia funkcjonowania aplikacji fragmentów kodu.
	
Kolejna część pracy poświęcona jest opisowi aplikacji typu klient dla urządzeń mobilnych pracujących pod kontrolą systemu Android. Po przedstawieniu istotnych informacji odnośnie dostępności omawianego rozwiązania zaprezentowany zostaje projekt interfejsu użytkownika wraz z towarzyszącymi mu funkcjonalnościami. Zawarte tam informacje mają na celu opisanie możliwie wszystkich sytuacji, z jakimi może spotkać się użytkownik w trakcie użytkowania aplikacji. Poruszony zostanie także charakterystyczny dla systemu Android cykl życia aktywności (ekranów programu) i sposób zarządzania nim w niniejszym projekcie.
	
Mając na uwadze łatwość późniejszej implementacji omawianego projektu na rzeczywistym sprzęcie w jednym z ostatnich rozdziałów opisane zostały stawiane mu wymagania jak również sposób instalacji oprogramowania tj. aplikacji serwerowej oraz klienckiej.
	
Praca pokazuje zasadność zaproponowanego rozwiązania ułatwiającego proces sterowania kołowym robotem mobilnym jak również wskazuje na potencjał współpracy systemów ROS oraz Android, który może być wykorzystany do dalszej modyfikacji i usprawniania omawianych aplikacji.

# Author / Autor #

Cezary Żelisko  
cezary.zelisko@gmail.com

# Supervisor #

[Daniel Koguciuk](http://www.linkedin.com/in/danielkoguciuk/)  
daniel.koguciuk@gmail.com